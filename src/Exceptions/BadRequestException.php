<?php

namespace Ifornew\Socialite\Exceptions;

/**
 * Class BadRequestException.
 */
class BadRequestException extends Exception
{
}
