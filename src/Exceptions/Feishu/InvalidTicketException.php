<?php

namespace Ifornew\Socialite\Exceptions\Feishu;

use Ifornew\Socialite\Exceptions\Exception;

/**
 * Class BadRequestException.
 */
class InvalidTicketException extends Exception
{
}
