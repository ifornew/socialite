<?php

namespace Ifornew\Socialite\Exceptions;

class MethodDoesNotSupportException extends Exception
{
    //
}
