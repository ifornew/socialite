<?php

namespace Ifornew\Socialite\Contracts;

interface FactoryInterface
{
    /**
     * @param string $driver
     *
     * @return \Ifornew\Socialite\Contracts\ProviderInterface
     */
    public function create(string $driver): ProviderInterface;
}
